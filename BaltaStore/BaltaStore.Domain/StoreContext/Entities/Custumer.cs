using System;
using System.Collections.Generic;
using System.Linq;
using BaltaStore.Domain.StoreContext.Enums.ValueObjects;

namespace BaltaStore.Domain.StoreContext.Entities
{
    public class Custumer
    {
        private readonly IList<Address> _address;
        public Custumer(
            Name name,
            Document document,
            Email email,
            string phone)
        {
            Name = name;
            Document = document;
            Email = email;
            Phone = phone;
            _address = new List<Address>();
        }

        public Name Name { get; set; }
        public Document Document { get; private set; }
        public Email Email { get; private set; }
        public string Phone { get; private set; }
        public IReadOnlyCollection<Address> Address => _address.ToArray();

        public void AddAddress(Address addAddress)
        {
            _address.Add(addAddress);
        }

        public override string ToString(){
            return Name.ToString();
        }
    }
}