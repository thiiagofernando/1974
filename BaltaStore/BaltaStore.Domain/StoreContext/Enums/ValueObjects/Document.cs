namespace BaltaStore.Domain.StoreContext.Enums.ValueObjects
{
    public class Document
    {
        public Document(string number)
        {
            Number = number;
        }
        public string Number { get; set; }

        public override string ToString(){
            return Number;
        }
    }
}