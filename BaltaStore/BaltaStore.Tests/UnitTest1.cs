using BaltaStore.Domain.StoreContext.Entities;
using BaltaStore.Domain.StoreContext.Enums.ValueObjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BaltaStore.Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            var name = new Name("Thiago", "Fernando");
            var document = new Document("01746592109");
            var email = new Email("thiago.fernando@msn.com");


            var c = new Custumer(name,document,email,"122342444");
            var mouse = new Product("Mouse","Rato","image.png",69.45M,10);
            var teclado = new Product("Keyboard", "Teclado sem fio", "image3.png", 79.45M, 130);
            var impressora = new Product("Impressora", "Impressora", "print.png", 619.45M, 1);



            var order = new Order(c);
            order.AddItem(new OrderItem(mouse,4));
            order.AddItem(new OrderItem(teclado, 4));
            order.AddItem(new OrderItem(impressora, 4));

            //realizei o pedido
            order.Place();

            //Simular Pagamento
            order.Pay();

            //Simular envio
            order.Ship();

            //simular o cancelamento
            order.Cancel();
        }
    }
}
